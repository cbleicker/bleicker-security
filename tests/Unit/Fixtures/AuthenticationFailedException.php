<?php

namespace Tests\Bleicker\Security\Unit\Fixtures;

use Bleicker\Security\Exception\AbstractVoterException;

/**
 * Class AuthenticationFailedException
 *
 * @package Tests\Bleicker\Security\Unit\Fixtures
 */
class AuthenticationFailedException extends AbstractVoterException {

}
