<?php

namespace Bleicker\Security\Exception;

use Exception;

/**
 * Class InvalidVoterExceptionException
 *
 * @package Bleicker\Security\Exception
 */
class InvalidVoterExceptionException extends Exception {

}
