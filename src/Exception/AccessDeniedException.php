<?php

namespace Bleicker\Security\Exception;

use Bleicker\Security\Exception\AbstractVoterException as Exception;

/**
 * Class AccessDeniedException
 *
 * @package Bleicker\Security\Exception
 */
class AccessDeniedException extends Exception {

}
