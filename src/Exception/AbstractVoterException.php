<?php

namespace Bleicker\Security\Exception;

use Exception;

/**
 * Class AbstractVoterException
 *
 * @package Bleicker\Security\Exception
 */
abstract class AbstractVoterException extends Exception {

}
